#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int getRandomNumber(int min, int max)
{
	static const double fraction = 1.0 / (static_cast<double>(RAND_MAX) + 1.0);
	return static_cast<int>(rand() * fraction * (max - min + 1) + min);
}

void fillArray(int array[4][6], int row, int column)
{
	for (int i = 0; i < row; i++)
		for (int j = 0; j < column; j++)
			array[i][j] = getRandomNumber(0, 99);
}

void sortArray(int array[4][6], int row, int column)
{
	for (int x = 0; x < row; x++)
		for (int y = 0; y < column; y++)
			for (int i = 0; i < row; i++)
				for (int j = 0; j < column; j++)
					if (array[i][j] < array[x][y])
						swap(array[i][j], array[x][y]);
}

void print2dArray(int array[4][6], int row, int column)
{
	for (int i = 0; i < row; i++)
	{
		for (int j = 0; j < column; j++)
			cout << array[i][j] << "\t";
		cout << endl;
	}
	cout << endl;
}

int main()
{
	srand(static_cast<unsigned int>(time(0)));

	const int row = 4;
	const int column = 6;

	int arr1[row][column];

	fillArray(arr1, row, column);
	print2dArray(arr1, row, column);
	sortArray(arr1, row, column);
	print2dArray(arr1, row, column);

	const int length = 6;
	int arr2[length];

	for (int i = 0, x = 0; i < row; i++)
	{
		for (int j = 0; j < column, x < length; j++)
		{
			arr2[x] = arr1[i][j];
			cout << arr2[x] << " ";
			x++;
		}
	}

	cin.get();
	return 0;
}
